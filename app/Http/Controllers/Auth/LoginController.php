<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function login(Request $request){
        //validation the form data
        $this->validate($request,[
            'mobile'    => 'required|max:11|min:11',
            'password' =>'required|min:6']);
        //attempt to log in the user

        if(Auth::guard('web')->attempt(['mobile' => $request->mobile, 'password' => $request->password],$request->remember)){
            // if successful redirect it to ...
            return redirect()->intended(route('admin'));
        }

        // if unsuccessful
        return redirect()->back()->withInpu($request->only('mobile','remember'));
    }

    public function logout(Request $request)
    {
        Auth::guard('web')->logout();

        $request->session()->flush();

        $request->session()->regenerate();

        return redirect()->route('public');
    }
}
